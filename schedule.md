# Important dates and deadlines

### Project 1
* Classes: Feb 13, Feb 20, Feb 27, March 6, March 13, March 20

* **Submission deadline (report and code): March 27**

### Project 2

* Classes: March 27, April 24, May 1, May 8

* **Submission deadline (report and code): May 15**


### Final project

* Classes: May 15, May 22, May 29

* Choice of projects: May 15
* **Presentation: June 5**


