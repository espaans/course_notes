# Brief git survival guide

## Create a new repository in gitlab

### 1. Go to https://gitlab.kwant-project.org/dashboard/projects (and sign in if you aren't yet)
### 2. Now start creating a new project:
   If this is the first project you create on gitlab, the button is instead
   called "Create a project"
   ![](images/new_project_firsttime.png)
   Once you have created a project, later projects can be created with the
   green "New project"
   ![](images/new_project.png)
### 3. Give your project some descriptive name
   ![](images/create_project.png)
### 4. Click on "Create project"
### 5. In your new project, go to the settings button (with the gear, in the column at the left), and click on "Members"
   ![](images/where_are_settings.png)
### 6. Share your project with your project partners. Make sure you give them "Master" access.
   ![](images/add_members.png)
### 7. Share your project with the computational_physics_18 group
   ![](images/share_with_group.png)

## Working with git

With your new gitlab repository, you can now follow what is done in the [computing crash course on day 2](https://gitlab.kwant-project.org/computational_physics_19/computing_crash_course/tree/master/day2_afternoon).
